package db;

public class TestObj {

    private int announce_id = 0;
    private String link = "";
    private String area = "";
    private String price = "";

    public TestObj(){

    }

    public TestObj(int announce_id, String link, String area, String price) {
        this.announce_id = announce_id;
        this.link = link;
        this.area = area;
        this.price = price;
    }

    public int getAnnounce_id() {
        return announce_id;
    }

    public void setAnnounce_id(int announce_id) {
        this.announce_id = announce_id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
