package db;

import Parsing.Parser;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

public class MongoDB {

    public  MongoClient mongoClient;
    public  MongoDatabase db;
    public  MongoCollection<Document> dbCollection;
    public Document found;
    public Document document;
    private  AtomicInteger count = new AtomicInteger(0);
    private String link;
    private String area;
    private String price;

    public MongoDB(){

        mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        db = mongoClient.getDatabase("Autodata");
        dbCollection = db.getCollection("krisha");

    }
    public void save_data(int id, String link, String area, String price) {

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss").format(Calendar.getInstance().getTime());


        found = dbCollection.find(new Document("An_id", id)).first();
        /*found = dbCollection.find(new Document("An_id", id)).first();*/
        document = new Document("An_id", id);
        document.append("Link", link);
        document.append("Area", area + " м²");
        document.append("Price", price);
        document.append("Date", timeStamp);

        if(found == null)
            dbCollection.insertOne(document);

    }

    public void print(String link, String area, String price, int num_of_ad){


        if (found == null) {

            this.link = link;
            this.area = area;
            this.price = price;
            num_of_ad = count.incrementAndGet();
            System.out.println(num_of_ad + ".Ссылка: " + link + "\n" + "Площадь: " + area + " м²"
                    + "\n" + "Цена: " + price);

            System.out.println();

        }

    }

}
