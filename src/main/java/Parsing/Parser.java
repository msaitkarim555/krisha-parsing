package Parsing;

import config.RotatingIP;
import db.MongoDB;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import config.SiteURL;


public class Parser {

    private SiteURL siteURL = new SiteURL();
    private Document doc;
    private AtomicInteger count = new AtomicInteger(0);
    private String link;
    private String area;
    private String price;
    private int an_num;
    private int id;
    private int total_of_page;
    public MongoDB mongoDB = new MongoDB();
    private int num_of_ad;

    private ArrayList<String> arrayList = new ArrayList<>();
    private static Random random = new Random();
    private int index = random.nextInt (8);

    private String[] user_agents =

    {
        "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:23.0) Gecko/20100101 Firefox/23.0",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36",
                "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)",
                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36",
                "Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20140205 Firefox/24.0 Iceweasel/24.3.0",
                "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0",
                "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:28.0) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
    };

    private Thread thread;
    RotatingIP rotatingIP = new RotatingIP();
    public Parser(){

    }

    public void MainRent(){
        long startTime = System.nanoTime();

        for(int do_it = 1; do_it <= count_page(); do_it++) {

            System.out.println("In page " + do_it + ":\n");
            try {
                if(do_it % 30 == 0){
                    Thread.sleep(60000);
                }
                String url = siteURL.getURLofRentWithFilter() + do_it;

                doc = Jsoup.connect(url).userAgent(" Main has been compiled by a more recent version of the Java Runtime (class file version 60.0), this version of the Java Runtime only recognizes class file versions up to 55.0\n")
                        .referrer("https://www.mozilla.org/en-US/firefox/").ignoreHttpErrors(true).get();

                Elements data = doc.select("div.main-col")
                        .select("section.a-list.a-search-list.a-list-with-favs")
                        .select("div.a-card.a-storage-live.ddl_product.ddl_product_link") /*.not-colored.is-visible.is-checked*/
                        /*.select("div.a-card__inc")*/;
               // System.out.println(random_user);
                Elements body = doc.select("div.paid-labels");

                int size = data.size();

                for (int i = 0; i < size; i++) {

                    Element ee = body.eq(i).first();
                    boolean is_hot = false;

                    String s = ee.select("span").attr("class");
                    if (s.equals("fi-paid-hot")) {
                        is_hot = true;
                    }

                    Element image = data.select("a.a-card__image").eq(i).first();
                    link = image.attr("abs:href");

                    String area_f = data.select("a.a-card__title ")
                            .eq(i).first().text();

                    int cArea = Integer.parseInt(area_f.replaceAll("[\\D]", ""));

                    area = Integer.toString(cArea);
                    price = data.select("div.a-card__price")
                            .eq(i).first().text().replaceAll(" за всё", "");

                    String id_text = data.eq(i).first().attr("data-id");

                    id = Integer.parseInt(id_text);



                    mongoDB.save_data(id, link, area, price);
                    mongoDB.print(link, area, price, num_of_ad);

                    if(mongoDB.found == null) {

                        arrayList.add("Ссылка: " + link + "\n" + "Площадь: " + area + " м²"
                                + "\n" + "Цена: " + price + "\n\n");

                    }

                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

        }

        long endTime   = System.nanoTime();
        long totalTime = endTime - startTime;
        double total_time_in_seconds = (double)totalTime / 1_000_000_000.0;

        System.out.println(arrayList);
        System.out.println(total_time_in_seconds);

    }





    public int count_page()  {

        String url = siteURL.getURLofRentWithFilter();

        try {
            doc = Jsoup.connect(url).userAgent(" Main has been compiled by a more recent version of the Java Runtime (class file version 60.0), this version of the Java Runtime only recognizes class file versions up to 55.0")
                    .referrer("https://www.mozilla.org/en-US/firefox/").ignoreHttpErrors(true).get();

        Elements pages = doc.select("nav.paginator").select("a.paginator__btn");
        int [] page_num_array = new int[pages.size()];
        total_of_page = page_num_array[0];
        for (int i = 0; i < pages.size(); i++){

            String page_num_s = pages.eq(i).first().attr("data-page");
            int page_num = Integer.parseInt(page_num_s);
            page_num_array[i] = page_num;

            if (page_num_array[i] > total_of_page) {
                total_of_page = page_num_array[i];
            }

        }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return total_of_page;
    }

    public ArrayList<String> getArrayList(){
        return arrayList;
    }

    public void setArrayList(ArrayList<String> arrayList){
        this.arrayList = arrayList;
    }
    public String getLink(){
        return link;
    }

    public String getArea(){
        return area;
    }

    public String getPrice(){
        return price;
    }

    public void setLink(String link){
        this.link = link;
    }

    public void setArea(String area){
        this.area = area;
    }

    public void setPrice(String price){
        this.price = price;
    }

    public int getAn_num(){
        return an_num;
    }

    public void setAn_num(int an_num){
        this.an_num = an_num;
    }





}
