package telegram_bot;

import Parsing.Parser;
import db.MongoDB;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.BotOptions;
import org.telegram.telegrambots.meta.generics.BotSession;
import org.telegram.telegrambots.meta.generics.LongPollingBot;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.ArrayList;
import java.util.Locale;


public class Bot extends TelegramLongPollingBot {

    private String link;
    private String area;
    private String price;
    private int an_num;
    private MongoDB mongoDB;
    /*private Parser parser = new Parser();*/
    private ArrayList<String> arrayList = new ArrayList<String>();
    private ArrayList<MessageEntity> messageEntityArrayList = new ArrayList<>();
    private Object[] array;
    boolean isActive = false;
    Parser parser;
    Thread thread;
    public Bot(){
        parser = new Parser();
    }

    public ArrayList<String> getArrayList(){
        return arrayList;
    }
    public void setArrayList(ArrayList<String> arrayList){
        this.arrayList = arrayList;
    }

    public void start() {

        try {
            TelegramBotsApi botsApi =
                    new TelegramBotsApi(
                            DefaultBotSession.class);
            botsApi.registerBot(new Bot());
        } catch (TelegramApiException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(parser.getArrayList());
    }

    BotSession botSession = new BotSession() {
        @Override
        public void setOptions(BotOptions botOptions) {

        }

        @Override
        public void setToken(String s) {

        }

        @Override
        public void setCallback(LongPollingBot longPollingBot) {

        }

        @Override
        public void start() {

        }

        @Override
        public void stop() {

        }

        @Override
        public boolean isRunning() {
            return false;
        }
    };


    public void sendDataToChannel(long channelChatID, String channelMessage){

        thread = new Thread(){

            public void run(){
                while(true){
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.setChatId(Long.toString(channelChatID));
                    parser.MainRent();
                    if(!parser.getArrayList().isEmpty()) {

                        sendMessage.setText(parser.getArrayList().toString().replaceAll(", ", "").replaceAll("]", "").replaceAll("\\[", ""));


                   /* else
                        sendMessage.setText("No new announcements");*/
                        try {
                            execute(sendMessage);
                        } catch (TelegramApiException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    parser.getArrayList().clear();
                }
            }

        };
        thread.start();

    }

    public void showStatus(long channelChatID){

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(Long.toString(channelChatID));

        if(isActive = true)
            sendMessage.setText("Bot is Activated");
        else {
            sendMessage.setText("Bot is not Activated");
        }

        try{
            execute(sendMessage);
        } catch (TelegramApiException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        long channelChatID = update.getChannelPost().getChatId();
        String channelMessage = update.getChannelPost().getText();
        int channelMessageID = update.getChannelPost().getMessageId();
        Message message = update.getMessage();

        if (channelMessage.toLowerCase(Locale.ROOT).equals("start bot")) {
            isActive = true;
            sendDataToChannel(channelChatID, channelMessage);


        } else if(channelMessage.toLowerCase(Locale.ROOT).equals("stop bot")){
            isActive = false;
        }

        if(channelMessage.equals("status")){
            showStatus(channelChatID);
        }

    }

    @Override
    public String getBotUsername() {
        return "Krisha_Parsing_bot";
    }

    @Override
    public String getBotToken() {
        return "1877982998:AAFAtCr45xNuiNJSHPLm62FUqrgQbnMYrag";
    }
}
